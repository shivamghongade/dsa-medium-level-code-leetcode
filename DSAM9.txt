class Solution {
    public void gameOfLife(int[][] arr) {
        int n=arr.length;
        int m=arr[0].length;
        int a[]= new int[]{-1,-1,-1,0,0,1,1,1};
        int b[]= new int[]{-1,0,1,-1,1,-1,0,1};
        for(int i=0;i<n;i++)
        {
            for(int j=0;j<m;j++)
            {
                int count=0;
                for(int p=0;p<8;p++)
                {
                    if(check(i+a[p],j+b[p],arr) && (arr[i+a[p]][j+b[p]]==1 || arr[i+a[p]][j+b[p]]==2 || arr[i+a[p]][j+b[p]]==3) )
                        count++;
                }
                if((arr[i][j]==0 || arr[i][j]==4 || arr[i][j]==5) && count==3)
                {
                    arr[i][j]=5;
                }
                else if(arr[i][j]==1 || arr[i][j]==2 || arr[i][j]==3)
                {
                    if(count<2)
                        arr[i][j]=3;
                    else if(count==2 || count==3)
                        arr[i][j]=2;
                    else if(count>3)
                        arr[i][j]=3;
                }
            }
        }
        for(int i=0;i<n;i++)
        {
            for(int j=0;j<m;j++)
            {
                if(arr[i][j]==2 || arr[i][j]==5)
                    arr[i][j]=1;
                else if(arr[i][j]==3 || arr[i][j]==4)
                    arr[i][j]=0;
            }
        }
        
    }
    
    public boolean check(int i, int j,int arr[][])
    {
        int n=arr.length,m=arr[0].length;
        if(i<0 || i>=n || j<0 || j>=m)
            return false;
        return true;
    }
}